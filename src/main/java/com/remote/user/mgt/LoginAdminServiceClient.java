package com.remote.user.mgt;
import java.rmi.RemoteException;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ServiceContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.wso2.carbon.authenticator.stub.AuthenticationAdminStub;
import org.wso2.carbon.authenticator.stub.LoginAuthenticationExceptionException;
import org.wso2.carbon.authenticator.stub.LogoutAuthenticationExceptionException;

public class LoginAdminServiceClient {

	private final String serviceName = "AuthenticationAdmin";
	private AuthenticationAdminStub authenticationAdminStub;
	private String endPoint;
	private String hostName;

	/**
	 * 
	 * @param url = "https://localhost:8080" 
	 * @param hostName = "localhost"
	 * @throws AxisFault
	 */
	public LoginAdminServiceClient(String url, String hostName) throws AxisFault {
		this.hostName = hostName;
		this.endPoint = url + "/services/" + serviceName;
		authenticationAdminStub = new AuthenticationAdminStub(endPoint);
	}

	/**
	 * 
	 * @param userName
	 * @param password
	 * @return "Session Cookie"
	 * @throws RemoteException
	 * @throws LoginAuthenticationExceptionException
	 */
	public String authenticate(String userName, String password) throws RemoteException, LoginAuthenticationExceptionException {
		String sessionCookie = null;
		if (authenticationAdminStub.login(userName, password, hostName)) {
			System.out.println("Login Successful");
			ServiceContext serviceContext = authenticationAdminStub._getServiceClient().getLastOperationContext().getServiceContext();
			sessionCookie = (String) serviceContext.getProperty(HTTPConstants.COOKIE_STRING);
			System.out.println(sessionCookie);
		}
		return sessionCookie;
	}

	/**
	 * Logout from the IDM
	 * @throws RemoteException
	 * @throws LogoutAuthenticationExceptionException
	 */
	public void logOut() throws RemoteException,
			LogoutAuthenticationExceptionException {
		authenticationAdminStub.logout();
	}
	
	public static void main(String[] args) throws RemoteException, LogoutAuthenticationExceptionException {
		LoginAdminServiceClient loginAdminServiceClient = new LoginAdminServiceClient("https://dev-idm.e-pragati.in", "dev-idm.e-pragati.in");
//		loginAdminServiceClient.authenticate("admin", password)
		loginAdminServiceClient.logOut();
	}
}
